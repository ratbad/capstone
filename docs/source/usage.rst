Usage
=====

Installation
------------

To use this, first install it using pip: 

.. code-block:: console

   (.venv) $ pip install table-maker
