Project Problem Definition
==========================

* Low-G events mostly effect lightweight helicopters in the civilian sector that use unslung, dual-bladed semi-rigid rotor designs, such as the Robinson R22, R44, R66 or Bell 206

* In these helicopters, Low-G events can cause 'mast-bumping', often fatal

* Causes are primarily pilot-induced

*Pushover event*

.. image:: ../../images/pushover.png
   :width: 500
   :alt: Diagram of a pushover event in a helicopter

*Diagram of Mast Bumping*

.. image:: ../../images/mast-bump.png
   :width: 400
   :alt: diagram of mass bumping on a helicopter rotor


#. **Collecting and labeling data**

The primary hurdle in the project is assembling a training data set: *sample data no longer provided to interns* 

Several data sources - each with challenges:

   * Hand-written flight logs

       * challenge: Low-G events often not annotated

   * Simulator data: S-76 & R22 platforms

      * challenge: Difficult to schedule simulator time

   * Historic flight data

      * challenge: big data, data profile for low-G events not yet discovered
   
.. image:: ../../images/flight-log.png
   :width: 600
   :alt: Example of a hand-written flight log, the unstructured data we work with for the project


**Data preparation and cleaning**

   #. Isolate variables that suggest low-G conditions:

      * rotor rpm

      * climb or descent rate

      * air speed

      * roll rate

      * yaw rate

   #. Devise a method for identifying low-G:

      * visual method: coincidence in variables on line plot

      * quantitative method: standard deviations above the mean


   #. Automate identifcation of low-G in historic flights

   
.. image:: ../../images/climbdescent.png
   :width: 500
   :alt: A correlation of max climb and descent rates in a line graph, suggesting a low-G event

Create row-by-row differences:


.. code-block:: Python

  # derive vertical acceleration from climb/descent rate over time
  df['vert_accel'] = (df.desc - df.desc.shift(1)) / (df.secs - df.secs.shift(1))

  # alternatively create row-by-row differences for all variables: 
  diff_cols = {'_'.join((col, 'diff')): df[col].diff() for col in df.columns.to_list()}
  diff_df = pd.DataFrame() # ugly but functional initialize-then-modify anti-pattern
  for col, series in diff_cols.items(): 
      diff_df[col] = pd.Series(series)

Assemble training data set: 

.. code-block:: Python
   :linenos:

   """Query all flights from 2023 using Athena,
   read them in as dataframes, run the low-G inspection 
   method on each"""

   from re import sub 
   from functools import partial
   import pandas as pd
   import awswrangler as aw 
   
   query_athena = partial(aw.athena.read_sql, database='universityose')

   flights_2023 = query_athena(
       sql_query=''.join((
       'SELECT DISTINCT(flight_id)', 
       "FROM 'phase_of_flight'", 
       "WHERE 'year' = '2023';"))
   )

   timestamps = [
       tuple(flight_id.split('_')) for flight_id in 
       [line[7:] for line in flights_2023.split()]
   ]

   columns_and_aliases = { # modify these as needed
       "times_gpsdatetime": "gps_time", 
       "times_seconds":"seconds",
       "flightstate_altitudes_gpsaltitude":"altitude", 
       "flightstate_position_pitch":"pitch", 
       "flightstate_position_roll":"roll",
       "flightstate_speeds_trueairspeed":"speed", 
       "flightstate_position_yaw":"yaw",
       "flightstate_rates_climbordescentrate":"descent"
   }

   def generate_sql(timestamp_pair: tuple) -> str:
     t1 = timestamp_pair[0]
     t2 = timestamp_pair[1]
     year = t1[:4]
     month = t1[4:6]
     hday1 = t1[-6:]
     hday2 = t2[-6:]
     ts1 = ':'.join((hday1[-6:-4], hday1[-4:-2], hday1[-2:]+'.000'))
     ts2 = ':'.join((hday2[-6:-4], hday2[-4:-2], hday2[-2:]+'.000'))
     sql_query = '\n'.join((
         'SELECT', 
         *[f'    {k} AS {v},' for k, v in columns_and_aliases.items()], 
         "FROM 'flight_details'", 
         f"WHERE 'year' = '{year}'",
         f"AND 'month' = '{month}'",
         f"AND 'times_gpsdatetime' > cast({ts1}) as timestamp)",
         f"AND 'times_gpsdatetime' < cast({ts2}) as timestamp)\n"))
     return sql_query
   # A SQL error if there is a comma following the last column in the select statement 
   
   queries = [
       sub(',\nFROM', '\nFROM', string) for string in 
       map(generate_sql, timestamps)
   ]
   
   for query in queries: 
       query_athena(sql_query=query)
       # insert low-G identification logic here: either visual method or quantitative or both

   
**Modeling Options**

   * Primary

       #. Ensemble methods

       #. SVM

       #. Naive Bayes

   * Alternate 

       #. DBSCAN 

