.. data670_presentation1 documentation master file, created by
   sphinx-quickstart on Thu Sep 21 21:23:01 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DATA 670: Presentation 2
========================
16 October 2023


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   data_analysis_project
   project_problem_def
   background
   importance
   kpi
   data_set_description
   data_review
   lessons_learned
   next_steps
   references
   questions


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
