Data Analysis Project
=====================

The data analytics problem that I am analyzing is **detecting Low-G events in helicopter flight data**

* Federal Aviation Administration's Aviation Research Division

* End goal: deliver a 'metric' for identifying low-G events 

* Embedded device providing real-time analysis on flight sensor data to predict possible loss of control events, warn pilots.

* Using Python: 
      * awswrangler: data access
      * Pandas: data preparation 
      * imblearn: model development
      * Sphinx: documentation
