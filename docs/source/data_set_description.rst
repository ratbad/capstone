Data Set Description
====================

#. Unstructured data 
   
   * 123 hand-written pdf logs,  2 to 11 pages in length

#. Flight simulator data
   
   * Flights scheduled for last week of September - postponed

#. **Historic flight data**

   * Thousands of flights of 50,000+ rows and up to 500 columns (<200 not null)
