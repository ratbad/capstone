Background
==========

* Georgia Institute of Technology - Center of Excellence for Helicopter Research

|

* Main research team: Chin, Payan, Puranik, **Johnson**, & Mavris.

  * 2019: *Phases of flight identification for rotorcraft operations*

  * 2017: *Anomaly detection in initial climb segments for helicopter operations*

  * 2017: *Helicopter approach stability analysis using flight data records*

|

* FAA internship history: Developing a catalog of predictive metrics that can accurately classify maneuvers and loss of control events

  |

  * Maneuvers/events covered: traffic patterns, quick stops, dynamic rollover, hover, low-g

  |

  * No metrics delivered, models not robust enough or used flight control predictors

  |

  * Feature selection most important contribution
