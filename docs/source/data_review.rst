Describe the Data 
=================

Highly imbalanced dataset

High Level Diagram
------------------

.. image:: ../../images/diagram.png
  :width: 500
  :alt: High-level data diagram


Sample Training Data
--------------------

.. list-table:: Sample training data
   :widths: 25 25 
   :header-rows: 1

   * - Feature
     - Data Type
   * - Airspeed(True)
     - float
   * - Altitude(AGL)
     - float
   * - **Altitude(MSL)**
     - float
   * - CollectivePos-[0]  
     - float
   * - **CyclicPitchPos-[0]**
     - float 
   * - GrossWeight
     - float 
   * - Label
     - int 
   * - PedalPos
     - float  
   * - Pitch
     - float 
   * - PitchAcceleration
     - float 
   * - Roll
     - float 
   * - RollAcceleration
     - float 
   * - RotorRPM-[0]
     - float 
   * - RotorTorque-[0]
     - float 
   * - SideslipAngle
     - float 
   * - Vert.Speed
     - float
   * - **Yaw**
     - float 
   * - YawAcceleration
     - float


Phase of Flight Data
--------------------

474 variables, many null

.. code-block:: sql

  SELECT * FROM information_schema.columns 
  WHERE table_schema = 'universityose_db' 
      AND table_name = 'phase_of_flight';


Potentially vital variables:

#. Flightstate_accelerations_verticalacceleration 

#. Flightstate_rates_climbordescentrate
   
.. list-table:: Phase of Flight data
   :widths: 40 10 
   :header-rows: 1

   * - column_name 
     - data_type
   * - approach_event_id_no_weak_dur 
     - varchar
   * - _id 
     - varchar
   * - mask 
     - varchar
   * - airdatacomputer_hasadc1validcopilot 
     - boolean
   * - airdatacomputer_hasadc1validpilot 
     - boolean
   * - airdatacomputer_hasadc2validcopilot 
     - boolean
   * - airdatacomputer_hasadc2validpilot 
     - boolean
   * - airframe_grossweight 
     - double
   * - airframe_hascabinheaterenabled 
     - boolean
   * - airframe_hascargohookload 
     - boolean
   * - airframe_hascontactwithtakeoffpad 
     - boolean
   * - airframe_hasfloatsdeployed 
     - boolean
   * - airframe_hasfloatsswitchenabled 
     - boolean
   * - airframe_haslandinggeardown 
     - boolean
   * - airframe_haslg1down 
     - boolean
   * - airframe_haslg2down 
     - boolean
   * - airframe_haslg3down 
     - boolean
   * - airframe_haslg4down 
     - boolean
   * - airframe_hasweightonwheels 
     - boolean
   * - airframe_haswheelwellfire 
     - boolean
   * - alerts_hasahrsfailurecaution 
     - boolean
   * - alerts_hasavionictorquemiscomparecaution 
     - boolean
   * - alerts_hasbatterywarning 
     - boolean
   * - alerts_hasdcgenerator1failure 
     - boolean
   * - alerts_hasdcgenerator2failure 
     - boolean
   * - alerts_haselectricdcbusfailurecaution 
     - boolean
   * - alerts_hasenginechipcaution 
     - boolean
   * - alerts_hasenginehotstartcaution 
     - boolean
   * - alerts_hasenhancedgpwsalert 
     - boolean
   * - alerts_hasessentialbusfailure 
     - boolean
   * - alerts_hasflightdirectorwarning 
     - boolean
   * - alerts_hasfuellowcaution 
     - boolean
   * - alerts_hasfuellowfailurecaution 
     - boolean
   * - alerts_hasgeneratorfailurewarning 
     - boolean
   * - alerts_hasgpwsalert 
     - boolean
   * - alerts_hashydraulicsystemwarning 
     - boolean
   * - alerts_haslowvacuumwarning 
     - boolean
   * - alerts_hasmastercaution 
     - boolean
   * - alerts_hasmasterwarning 
     - boolean
   * - alerts_hasnonessentialbusfailure 
     - boolean
   * - alerts_haspilotevent 
     - boolean
   * - alerts_hassasfault 
     - boolean
   * - alerts_hasstallhornenabled 
     - boolean
   * - alerts_hastailrotorchipwarning 
     - boolean
   * - alerts_hastransmissionchipwarning 
     - boolean
   * - alerts_hastransmissionigbchipcaution 
     - boolean
   * - alerts_hastransmissionmaingearboxoillowcaution 
     - boolean
   * - alerts_hastransmissionoilpressurewarning 
     - boolean
   * - alerts_hastransmissionoiltemperaturewarning 
     - boolean
   * - atmospheric_calculatedwinddirection 
     - double
   * - atmospheric_calculatedwinddirectionbodyframe 
     - double
   * - atmospheric_calculatedwindspeed 
     - double
   * - atmospheric_densityaltitude 
     - double
   * - atmospheric_haspitotheatenabled 
     - boolean
   * - atmospheric_ltealtitudeloss 
     - boolean
   * - atmospheric_ltewinddirectionrangemaxbf 
     - double
   * - atmospheric_ltewinddirectionrangeminbf 
     - double
   * - atmospheric_outsideairdensity 
     - double
   * - atmospheric_outsideairpressure 
     - double
   * - atmospheric_outsideairtemperature 
     - double
   * - atmospheric_outsidedewpoint 
     - double
   * - atmospheric_pitotstaticwindspeed 
     - double
   * - atmospheric_surfacewinddirection 
     - double
   * - atmospheric_surfacewindspeed 
     - double
   * - atmospheric_vil 
     - double
   * - autopilot_attitudeholdmode 
     - boolean
   * - autopilot_autopilotcoupledmode 
     - boolean
   * - autopilot_autopilotmode 
     - varchar
   * - autopilot_autopilotpitchattitude 
     - double
   * - autopilot_autopilotrollattitude 
     - double
   * - autopilot_collectiveflightdirectormode 
     - boolean
   * - autopilot_collectiveradaltmode 
     - boolean
   * - autopilot_hasap1enabled 
     - boolean
   * - autopilot_hasap2enabled 
     - boolean
   * - autopilot_hasautopilotdisengaged 
     - boolean
   * - autopilot_hassasengaged 
     - boolean
   * - autopilot_lateralmodes_backcoursemode 
     - boolean
   * - autopilot_lateralmodes_headingmode 
     - boolean
   * - autopilot_lateralmodes_navmode 
     - boolean
   * - autopilot_selectedaltitude 
     - double
   * - autopilot_verticalmodes_airspeedmode 
     - boolean
   * - autopilot_verticalmodes_alititudepremode 
     - boolean
   * - autopilot_verticalmodes_altitudemode 
     - boolean
   * - autopilot_verticalmodes_decelmode 
     - boolean
   * - autopilot_verticalmodes_glidescopeannunciatormode 
     - boolean
   * - autopilot_verticalmodes_ilsmode 
     - boolean
   * - autopilot_verticalmodes_standbymode 
     - boolean
   * - autopilot_verticalmodes_verticalspeedmode 
     - boolean
   * - autopilot_verticalmodes_vorapproachmode 
     - boolean
   * - electrical_acvoltage1 
     - double
   * - electrical_acvoltage2 
     - double
   * - electrical_batterycurrent1 
     - double
   * - electrical_batterycurrent2 
     - double
   * - electrical_hasinverterfailure 
     - boolean
   * - electrical_haslowvoltagewarning 
     - boolean
   * - engines_computations_e1powermargin 
     - double
   * - engines_computations_e1t5margin 
     - double
   * - engines_computations_e1torque 
     - double
   * - engines_computations_e1torquedifference 
     - double
   * - engines_computations_e2powermargin 
     - double
   * - engines_computations_e2t5margin 
     - double
   * - engines_computations_e2torque 
     - double
   * - engines_computations_e2torquedifference 
     - double
   * - engines_e1temperature 
     - double
   * - engines_e2temperature 
     - double
   * - engines_fuelremaining 
     - double
   * - engines_fuelsystem_e1fuelflowrate 
     - double
   * - engines_fuelsystem_e1fuelpressuregauge 
     - double
   * - engines_fuelsystem_e2fuelflowrate 
     - double
   * - engines_fuelsystem_e2fuelpressuregauge 
     - double
   * - engines_fuelsystem_hase1fuelpressurewarning 
     - boolean
   * - engines_fuelsystem_hase1lowfuellevelwarning 
     - boolean
   * - engines_fuelsystem_hase1pumpfailurewarning 
     - boolean
   * - engines_fuelsystem_hase2fuelpressurewarning 
     - boolean
   * - engines_fuelsystem_hase2lowfuellevelwarning 
     - boolean
   * - engines_fuelsystem_hase2pumpfailurewarning 
     - boolean
   * - engines_fueltank1weight 
     - double
   * - engines_fueltank2weight 
     - double
   * - engines_hase1enginechipwarning 
     - boolean
   * - engines_hase1enginefailure 
     - boolean
   * - engines_hase1firewarning 
     - boolean
   * - engines_hase2enginechipwarning 
     - boolean
   * - engines_hase2enginefailure 
     - boolean
   * - engines_hase2firewarning 
     - boolean
   * - engines_oil_e1oilpressure 
     - double
   * - engines_oil_e1oiltemperature 
     - double
   * - engines_oil_e2oilpressure 
     - double
   * - engines_oil_e2oiltemperature 
     - double
   * - engines_pistonengines_e1manifoldpressure 
     - double
   * - engines_pistonengines_e1rpm 
     - double
   * - engines_pistonengines_e2manifoldpressure 
     - double
   * - engines_pistonengines_e2rpm 
     - double
   * - engines_totalfuelweight 
     - double
   * - engines_totaltorque 
     - double
   * - engines_turbineengines_e1itt 
     - double
   * - engines_turbineengines_e1measuredgastemperature 
     - double
   * - engines_turbineengines_e1n1ng 
     - double
   * - engines_turbineengines_e1n2nf 
     - double
   * - engines_turbineengines_e1staticpressure 
     - double
   * - engines_turbineengines_e1totalpressure 
     - double
   * - engines_turbineengines_e2itt 
     - double
   * - engines_turbineengines_e2measuredgastemperature 
     - double
   * - engines_turbineengines_e2n1ng 
     - double
   * - engines_turbineengines_e2n2nf 
     - double
   * - engines_turbineengines_e2staticpressure 
     - double
   * - engines_turbineengines_e2totalpressure 
     - double
   * - flightcontrols_antitorquepedalposition 
     - double
   * - flightcontrols_collectiveforce 
     - double
   * - flightcontrols_collectivepitchtrim 
     - double
   * - flightcontrols_collectiveposition 
     - double
   * - flightcontrols_controllablestabilatorposition 
     - double
   * - flightcontrols_cyclicforce 
     - double
   * - flightcontrols_cyclicpitchtrim 
     - double
   * - flightcontrols_cyclicposition 
     - double
   * - flightcontrols_cyclicpositionpitch 
     - double
   * - flightcontrols_cyclicpositionroll 
     - double
   * - flightcontrols_cyclicrolltrim 
     - double
   * - flightcontrols_hasbleedairsystemswitchenabled 
     - boolean
   * - flightcontrols_hasparkingbrakesengaged 
     - boolean
   * - flightcontrols_hasrotorbrakeengaged 
     - boolean
   * - flightcontrols_pedalbrakes_leftbrakeposition 
     - double
   * - flightcontrols_pedalbrakes_rightbrakeposition 
     - double
   * - flightcontrols_pedalbrakes_hasleftwheelbrakesengaged 
     - boolean
   * - flightcontrols_pedalbrakes_hasrightwheelbrakesengaged 
     - boolean
   * - flightcontrols_pedalforce 
     - double
   * - flightcontrols_tailrotorbrakesposition 
     - double
   * - flightcontrols_throttleposition 
     - double
   * - flightdirector_copilotflightdirectormodes_coaltitudehold 
     - boolean
   * - flightdirector_copilotflightdirectormodes_coaltitudepreselectarmed 
     - boolean
   * - flightdirector_copilotflightdirectormodes_coaltitudepreselectcapture 
     - boolean
   * - flightdirector_copilotflightdirectormodes_codecelerationarmed 
     - boolean
   * - flightdirector_copilotflightdirectormodes_codecelerationcapture 
     - boolean
   * - flightdirector_copilotflightdirectormodes_coga 
     - boolean
   * - flightdirector_copilotflightdirectormodes_coglideslopearmed 
     - boolean
   * - flightdirector_copilotflightdirectormodes_coglideslopecapture 
     - boolean
   * - flightdirector_copilotflightdirectormodes_coheadingselect 
     - boolean
   * - flightdirector_copilotflightdirectormodes_coindicatedairspeedhold 
     - boolean
   * - flightdirector_copilotflightdirectormodes_conavigationarmed 
     - boolean
   * - flightdirector_copilotflightdirectormodes_conavigationcapture 
     - boolean
   * - flightdirector_copilotflightdirectormodes_coradalthold 
     - boolean
   * - flightdirector_copilotflightdirectormodes_coverticalspeedhold 
     - boolean
   * - flightdirector_copilotflightdirectormodes_covorapproacharmed 
     - boolean
   * - flightdirector_copilotflightdirectormodes_covorapproachcapture 
     - boolean
   * - flightdirector_flightdirectorcontrolmode 
     - varchar
   * - flightdirector_flightdirectormode 
     - varchar
   * - flightdirector_flightdirectorpitch 
     - double
   * - flightdirector_flightdirectorroll 
     - double
   * - flightdirector_flightdirectoryaw 
     - double
   * - flightdirector_hasfd1enabled 
     - boolean
   * - flightdirector_hasfd2enabled 
     - boolean
   * - flightdirector_pilotflightdirectormodes_altitudehold 
     - boolean
   * - flightdirector_pilotflightdirectormodes_altitudepreselectarmed 
     - boolean
   * - flightdirector_pilotflightdirectormodes_altitudepreselectcapture 
     - boolean
   * - flightdirector_pilotflightdirectormodes_decelerationarmed 
     - boolean
   * - flightdirector_pilotflightdirectormodes_decelerationcapture 
     - boolean
   * - flightdirector_pilotflightdirectormodes_ga 
     - boolean
   * - flightdirector_pilotflightdirectormodes_glideslopearmed 
     - boolean
   * - flightdirector_pilotflightdirectormodes_glideslopecapture 
     - boolean
   * - flightdirector_pilotflightdirectormodes_headingselect 
     - boolean
   * - flightdirector_pilotflightdirectormodes_indicatedairspeedhold 
     - boolean
   * - flightdirector_pilotflightdirectormodes_navigationarmed 
     - boolean
   * - flightdirector_pilotflightdirectormodes_navigationcapture 
     - boolean
   * - flightdirector_pilotflightdirectormodes_radalthold 
     - boolean
   * - flightdirector_pilotflightdirectormodes_verticalspeedhold 
     - boolean
   * - flightdirector_pilotflightdirectormodes_vorapproacharmed 
     - boolean
   * - flightdirector_pilotflightdirectormodes_vorapproachcapture 
     - boolean
   * - flightstate_accelerations_horizontalacceleration 
     - double
   * - flightstate_accelerations_lateralacceleration
     - double
   * - flightstate_accelerations_longitudinalacceleration 
     - double
   * - flightstate_accelerations_normalacceleration 
     - double
   * - flightstate_accelerations_pitchacceleration 
     - double
   * - flightstate_accelerations_rollacceleration 
     - double
   * - **flightstate_accelerations_verticalacceleration**
     - double
   * - flightstate_accelerations_yawacceleration 
     - double
   * - flightstate_altitudes_absolutealtitude 
     - double
   * - flightstate_altitudes_altimetersettingcopilot 
     - double
   * - flightstate_altitudes_altimetersettingpilot 
     - double
   * - flightstate_altitudes_approximatealtitude 
     - double
   * - flightstate_altitudes_barometricaltitudecopilot 
     - double
   * - flightstate_altitudes_barometricaltitudepilot 
     - double
   * - flightstate_altitudes_copilotradioaltitude 
     - double
   * - flightstate_altitudes_gpsaltitude 
     - double
   * - flightstate_altitudes_heightaboveellipsoid 
     - double
   * - flightstate_altitudes_indicatedaltitude 
     - double
   * - flightstate_altitudes_pilotradioaltitude 
     - double
   * - flightstate_altitudes_truealtitude 
     - double
   * - flightstate_location_coordinates 
     - varchar
   * - flightstate_location_latitude 
     - double
   * - flightstate_location_longitude 
     - double
   * - flightstate_location_type 
     - varchar
   * - flightstate_position_angleofattack 
     - double
   * - flightstate_position_compassheading 
     - double
   * - flightstate_position_deviation 
     - double
   * - flightstate_position_groundtrack 
     - double
   * - flightstate_position_groundtrackbodyframe 
     - double
   * - flightstate_position_horizontalflightpathangle 
     - double
   * - flightstate_position_magneticheading 
     - double
   * - flightstate_position_magneticvariance 
     - double
   * - flightstate_position_maneuver 
     - varchar
   * - flightstate_position_phaseofflight 
     - varchar
   * - flightstate_position_phaseofturn 
     - varchar
   * - flightstate_position_pitch 
     - double
   * - flightstate_position_roll 
     - double
   * - flightstate_position_sideslip 
     - double
   * - flightstate_position_trueheading 
     - double
   * - flightstate_position_verticalflightpathangle 
     - double
   * - flightstate_position_yaw 
     - double
   * - flightstate_rates_altituderate 
     - double
   * - **flightstate_rates_climbordescentrate**
     - double
   * - flightstate_rates_mainrotorinducedinflow 
     - double
   * - flightstate_rates_mainrotorinducedinflowmax 
     - double
   * - flightstate_rates_pitchrate 
     - double
   * - flightstate_rates_rollrate 
     - double
   * - flightstate_rates_turnrate 
     - double
   * - flightstate_rates_yawrate 
     - double
   * - flightstate_speeds_calibratedairspeed 
     - double
   * - flightstate_speeds_calibratedverticalspeed 
     - double
   * - flightstate_speeds_derivedindicatedairspeed 
     - double
   * - flightstate_speeds_equivalentairspeed 
     - double
   * - flightstate_speeds_groundspeed 
     - double
   * - flightstate_speeds_indicatedairspeed 
     - double
   * - flightstate_speeds_trueairspeed 
     - double
   * - flightstate_speeds_verticalspeedanemometer 
     - double
   * - fms_approachmode 
     - varchar
   * - fms_baroaltvalidandapproacharmed 
     - boolean
   * - fms_crosstrackdistance 
     - double
   * - fms_decisionheightcopilot 
     - double
   * - fms_decisionheightpilot 
     - double
   * - fms_driftangle 
     - double
   * - fms_filteredwinddirection 
     - double
   * - fms_filteredwindspeed 
     - double
   * - fms_hascrosstrackerror 
     - double
   * - fms_lateraldeviationscalefactor 
     - double
   * - fms_magneticheadingfms 
     - double
   * - fms_mode 
     - varchar
   * - fms_navwaypoint_waypointaltitude 
     - double
   * - fms_navwaypoint_waypointdistance 
     - double
   * - fms_navwaypoint_waypointlocation_coordinates 
     - varchar
   * - fms_navwaypoint_waypointlocation_latitude 
     - double
   * - fms_navwaypoint_waypointlocation_longitude 
     - double
   * - fms_navwaypoint_waypointlocation_type 
     - varchar
   * - fms_navwaypoint_waypointname 
     - varchar
   * - fms_navwaypoint_waypointnumber 
     - double
   * - fms_navwaypoint_waypointtime 
     - double
   * - fms_navwaypoint_waypointtruebearing 
     - double
   * - fms_rnpvalue 
     - varchar
   * - fms_selectedheading 
     - double
   * - fms_verticaldeviation 
     - double
   * - fms_verticaldeviationscalefactor 
     - double
   * - hydraulic_hydraulicoiltemperature 
     - double
   * - hydraulic_hydraulicpressureprimary 
     - double
   * - hydraulic_hydraulicpressuresecondary 
     - double
   * - hydraulic_hydraulicselectorswitchposition 
     - varchar
   * - navigation_enum 
     - double
   * - navigation_flightpathangle 
     - double
   * - navigation_glideslopedeviationcopilot 
     - double
   * - navigation_glideslopedeviationpilot 
     - double
   * - navigation_gnss_horizontalaccuracy 
     - double
   * - navigation_gnss_satelliteconstellationname 
     - varchar
   * - navigation_gnss_verticalaccuracy 
     - double
   * - navigation_gnss_verticalvelocity 
     - double
   * - navigation_gnss_visiblesatellites 
     - double
   * - navigation_gpshorizontaldeviation 
     - double
   * - navigation_hasnavigationvalid 
     - boolean
   * - navigation_headingchangewithinduration 
     - double
   * - navigation_inertialnavigationunitposition_coordinates 
     - varchar
   * - navigation_inertialnavigationunitposition_latitude 
     - double
   * - navigation_inertialnavigationunitposition_longitude 
     - double
   * - navigation_inertialnavigationunitposition_type 
     - varchar
   * - navigation_localizerdeviation 
     - double
   * - navigation_lpvstatus 
     - boolean
   * - navigation_nav1horizontaldeviation 
     - double
   * - navigation_nav1verticaldeviation 
     - double
   * - navigation_nav2horizontaldeviation 
     - double
   * - navigation_nav2verticaldeviation 
     - double
   * - navigation_trajectoryroute 
     - varchar
   * - radios_commradio1 
     - varchar
   * - radios_commradio2 
     - varchar
   * - radios_isradiotransmitterkeying 
     - boolean
   * - radios_navradio1 
     - varchar
   * - radios_navradio2 
     - varchar
   * - rotors_hasmainrotorhighrpmwarning 
     - boolean
   * - rotors_hasmainrotorlowrpmwarning 
     - boolean
   * - rotors_hastailrotorhighrpmwarning 
     - boolean
   * - rotors_hastailrotorlowrpmwarning 
     - boolean
   * - rotors_mainrotorrpm 
     - double
   * - rotors_mainrotorrpmpercentage 
     - double
   * - rotors_mainrotortorque 
     - double
   * - rotors_rotorhuborientation 
     - double
   * - rotors_tailrotorrpm 
     - double
   * - rotors_tailrotorrpmpercentage 
     - double
   * - rotors_tailrotortorque 
     - double
   * - times_batteryoperatingtime 
     - varchar
   * - times_elapsedtime 
     - double
   * - times_engineoperatingtime 
     - varchar
   * - times_generatoraputime 
     - varchar
   * - times_gpsdatetime 
     - timestamp(3)
   * - times_hobbstime 
     - double
   * - times_internalclock 
     - varchar
   * - times_seconds 
     - double
   * - times_timestamp 
     - varchar
   * - transmissionsystem_maingearboxoilpressure 
     - double
   * - transmissionsystem_maingearboxoiltemperature 
     - double
   * - transmissionsystem_tailrotorgearboxoilpressure 
     - double
   * - transmissionsystem_tailrotorgearboxoiltemperature 
     - double
   * - transmissionsystem_transmissionoilpressure 
     - double
   * - transmissionsystem_transmissionoiltemperature 
     - double
   * - final_agl 
     - double
   * - final_msl 
     - double
   * - terr_alt 
     - double
   * - t_stamp 
     - timestamp(3)
   * - unix_timestamp 
     - double
   * - timestamp2 
     - timestamp(3)
   * - verticalspeed_fpm 
     - double
   * - groundspeed_kt 
     - double
   * - verticalspeed_mavg3_fpm 
     - double
   * - verticalspeed_mavg1_fpm 
     - double
   * - verticalspeed_mavg5_fpm 
     - double
   * - verticalspeed_mavg10_fpm 
     - double
   * - groundspeed_mavg1_kt 
     - double
   * - groundspeed_mavg3_kt 
     - double
   * - groundspeed_mavg5_kt 
     - double
   * - groundspeed_mavg10_kt 
     - double
   * - verticalspeed_final_fpm 
     - double
   * - groundspeed_final_kt 
     - double
   * - vrs 
     - boolean
   * - lte 
     - integer
   * - phaseofflight_mavg3 
     - varchar
   * - phaseofflight_mavg5 
     - varchar
   * - phaseofflight_mavg10 
     - varchar
   * - phaseofflight_final 
     - varchar
   * - approach_icao_id 
     - varchar
   * - approach_landing_surface 
     - varchar
   * - approach_rwy_pad_id 
     - varchar
   * - approach_rwy_bearing_deg 
     - double
   * - approach_surface_msl 
     - double
   * - approach_distance_rwy_pad_ft 
     - double
   * - approach_agl 
     - double
   * - approach_event_ifr_trimmed_dur_s 
     - double
   * - approach_height_start_ifr_ft 
     - double
   * - approach_event_id_ifr 
     - varchar
   * - approach_height_start_vfr_ft 
     - double
   * - approach_id 
     - varchar
   * - approach_type 
     - varchar
   * - approach_altitude_gate 
     - varchar
   * - approach_stable_gate_airspeed_kt 
     - double
   * - approach_airspeed_smooth 
     - double
   * - approach_gate_airspeed_kts 
     - double
   * - approach_unstable_airspeed 
     - boolean
   * - approach_angle 
     - double
   * - approach_angle_class 
     - varchar
   * - approach_angle_classification 
     - varchar
   * - approach_unstable_angle 
     - boolean
   * - approach_vertspeed_ideal_fpm 
     - double
   * - approach_nominal_agl 
     - double
   * - approach_vertspeed_class 
     - varchar
   * - approach_vertspeed_classification 
     - varchar
   * - approach_unstable_vertspeed 
     - boolean
   * - approach_bearing_deg 
     - double
   * - approach_bearing_ideal_deg 
     - double
   * - approach_unstable_groundtrack 
     - boolean
   * - approach_unstable_landing 
     - boolean
   * - approach_unstable_bank 
     - boolean
   * - approach_tol_high_airspeed_kt 
     - double
   * - approach_norm_high_airspeed_kt 
     - double
   * - approach_norm_low_airspeed_kt 
     - double
   * - approach_tol_low_airspeed_kt 
     - double
   * - approach_tol_high_angle_deg 
     - double
   * - approach_norm_high_angle_deg 
     - double
   * - approach_norm_low_angle_deg 
     - double
   * - approach_tol_low_angle_deg 
     - double
   * - approach_tol_high_vertspeed_fpm 
     - double
   * - approach_norm_high_vertspeed_fpm 
     - double
   * - approach_norm_low_vertspeed_fpm 
     - double
   * - approach_tol_low_vertspeed_fpm 
     - double
   * - approach_tol_high_groundtrack_deg 
     - double
   * - approach_norm_high_groundtrack_deg 
     - double
   * - approach_norm_low_groundtrack_deg 
     - double
   * - approach_tol_low_groundtrack_deg 
     - double
   * - approach_norm_high_landing_kt 
     - double
   * - approach_norm_low_landing_kt 
     - double
   * - approach_norm_high_bank_deg 
     - double
   * - approach_norm_low_bank_deg 
     - double
   * - maneuver_hoverornot 
     - integer
   * - maneuver_rightturn 
     - integer
   * - maneuver_leftturn 
     - integer
   * - maneuver_turnabpcw 
     - integer
   * - maneuver_turnabpccw 
     - integer
   * - maneuver_hovermean 
     - double
   * - maneuver_rmean 
     - double
   * - maneuver_lmean 
     - double
   * - maneuver_cwmean 
     - double
   * - maneuver_ccwmean 
     - double
   * - fms_lateraldeviation 
     - double
   * - heading_final_deg 
     - double
   * - approach_airspeed_at_landing 
     - varchar
   * - approach_iap 
     - varchar
   * - approach_landing_icao_id 
     - varchar
   * - approach_procedure 
     - varchar
   * - approach_id_new 
     - varchar
   * - approach_type_new 
     - varchar
   * - approach_magneticvariation_deg 
     - varchar
   * - approach_routetype 
     - varchar
   * - approach_ap_id 
     - varchar
   * - approach_navaidclass 
     - varchar
   * - approach_navaidtype 
     - varchar
   * - approach_sectioncode 
     - varchar
   * - approach_subsectioncode 
     - varchar
   * - approach_navaidclassname 
     - varchar
   * - approach_rwyid 
     - varchar
   * - approach_stationdeclination_deg 
     - varchar
   * - approach_unstable_latdev_half 
     - boolean
   * - approach_unstable_latdev_one 
     - boolean
   * - approach_unstable_latdev_two 
     - boolean
   * - approach_unstable_latdev_three 
     - boolean
   * - approach_tol_high_ifr_latdev_deg 
     - double
   * - approach_norm_upper_ifr_latdev_deg 
     - double
   * - approach_norm_lower_ifr_latdev_deg 
     - double
   * - approach_tol_low_ifr_latdev_deg 
     - double
   * - approach_unstable_vertdev_one 
     - boolean
   * - approach_unstable_vertdev_two 
     - boolean
   * - approach_unstable_vertdev_three 
     - boolean
   * - approach_tol_high_ifr_gs_deg 
     - double
   * - approach_norm_upper_ifr_gs_deg 
     - double
   * - approach_norm_lower_ifr_gs_deg 
     - double
   * - approach_tol_low_ifr_gs_deg 
     - double
   * - approach_aircraft_to_navaid_bearing_degrees 
     - double
   * - approach_glideslope_degrees_mavg 
     - double
   * - approach_normal_upper_ifr_latdev_deg 
     - double
   * - approach_normal_upper_ifr_gs_deg 
     - double
   * - approach_normal_lower_ifr_gs_deg 
     - double
   * - flightid 
     - varchar
   * - year 
     - varchar
   * - month 
     - varchar
