Key Performance Indicators (KPIs)
=================================

#. Achieve milestones on time

   #. Research completed by 30 September

   #. Data collection compeleted by 14 October

   #. Analysis completed 28 October

   #. Deliverables finalized 4 November 

#. Detect Low-G events in historic and future flight data

   * If using supervised methods, emphasis on sensitivity/recall

      * Highly imbalanced, target variable = possibly 1/200 of the data

#. Comply with FAA metric standards

