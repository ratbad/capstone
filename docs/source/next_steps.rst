Next Steps
==========
#. Develop quantitative method

   * Standard deviations above mean or orders of magnitude 

   * Peaks within x time each other

   * Extract and label rows (maybe don't need entire flight?)


#. Scale method to all flights

#. Data conditioning

   * group by larger time scale, use averages?

   * convert units to feet per second

#. Begin model development 
