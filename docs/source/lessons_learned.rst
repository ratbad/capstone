Lessons Learned
===============
* Domain knowledge:

    * differences between rotor blade systems

    * helicopter flight physics & controls 

    * flight recorders and measurements

* New technologies:

    * AWS: Athena, S3

    *  *awswrangler*

* Some disagreement about low-G conditions: 

    * cyclic vs. collective 

    * yaw vs. roll

    * vertical acceleration vs. climb-or-descent


.. image:: ../../images/altitudes.png
   :width: 600
   :alt: line plot showing three different lines each of which is a different measure of altitude from the flight recorder data
