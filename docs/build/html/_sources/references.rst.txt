References
==========

* Chin H.J., Payan, A., Johnson, C., & Mavris, D.N.. (2017). Anomaly detection in initial climb segments for helicopter operations. *Journal of Aerospace Computing, Information, and Communication 15(169)*:1-14. https://doi.org/10.4050/F-0077-2021-16852

* Chin, H.J., Payan, A. P., Johnson, C., & Mavris, D. (2019). *Phases of flight identification for rotorcraft operations*. Georgia Institute of Technology. https://doi.org/10.2514/6.2019-0139

* El-Banna, M. (2017). Modified Mahalanobis Taguchi system for imbalance data classification. *Computational Intelligence & Neuroscience*, 1–15. 	https://doi-org.ezproxy.umgc.edu/10.1155/2017/5874896

* FAA-H-8083-21B: *Helicopter flying handbook*. (2019). United States Department of Transportation, Federal Aviation Administration.

* Kivak, R. (2023). *Key performance indicator (KPI)*. Salem Press Encyclopedia.

* Li, J., & Chao, S. (2023). A novel twin-support vector machines method for binary classification to imbalanced data. *Journal of Intelligent & Fuzzy Systems, 44* (4), 6901–6910. https://doi-org.ezproxy.umgc.edu/10.3233/JIFS-222501

* McKay, A. (March, 2015). Flight into turbulence. *New Zealand Helicopter Association Safety Bulletin 4*. 

* Payan, A., Lin, P.N., Johnson, C., & Mavris, D.N. (2017). *Helicopter approach stability analysis using flight data records*. Georgia Institute of Technology.

* Puranik. T.G. & Mavris, D.N. (2017). Anomaly detection in general-aviation operations using energy metrics and flight-data records. *Journal of Aerospace Information Systems, 15* (1). https://doi.org/10.2514/1.I010582

* Rockart, J.F. (1979). *Chief executives define their own data needs*. Harvard Business Review. 57, 81–93.

* Rotaru, C., & Todorov, M. (2018). Helicopter flight physics. In Volkov, K. (Ed.) *Flight Physics* eds: Volkov, K. DOI: 10.5772/intechopen.71516. IntechOpen.

* Smith, R. & Hawkins, B. (2004). *Lean maintenance: Reduce costs, improve quality, and increase market share*. Elsevier Butterworth-Heinemann.

* Sun, H., Xie, J., Jiao, Y., Huang, R., & Lu, B. (2020). Event detection and spatio-temporal analysis of low-altitude unstable approach. *Applied Sciences, 10* (14), 4934. https://doi.org/10.3390/app10144934

* Schwabish. J. (2021). *Better data visualizations : A guide for scholars, researchers, and wonks*. Columbia University Press.

* Xu Z., Saleh, J.H., & Sbagia, R. (2020). Machine learning for helicopter accident analysis using supervised classification: Inference, prediction, and implications. *Reliability Engineering & System Safety, 204*. https://doi.org/10.1016/j.ress.2020.107210




