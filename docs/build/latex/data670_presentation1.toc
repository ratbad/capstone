\babel@toc {english}{}\relax 
\contentsline {chapter}{\numberline {1}Data Analysis Project}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Project Problem Definition}{5}{chapter.2}%
\contentsline {chapter}{\numberline {3}Background}{7}{chapter.3}%
\contentsline {chapter}{\numberline {4}Importance of Project Reviewed}{9}{chapter.4}%
\contentsline {chapter}{\numberline {5}Key Performance Indicators (KPIs)}{11}{chapter.5}%
\contentsline {chapter}{\numberline {6}Data Set Description}{13}{chapter.6}%
\contentsline {chapter}{\numberline {7}Describe the Data}{15}{chapter.7}%
\contentsline {chapter}{\numberline {8}Lessons Learned}{19}{chapter.8}%
\contentsline {chapter}{\numberline {9}Next Steps}{21}{chapter.9}%
\contentsline {chapter}{\numberline {10}References}{23}{chapter.10}%
\contentsline {chapter}{\numberline {11}Questions?}{25}{chapter.11}%
\contentsline {chapter}{\numberline {12}Indices and tables}{27}{chapter.12}%
